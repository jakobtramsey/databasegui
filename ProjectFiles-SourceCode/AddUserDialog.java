import javax.swing.*;
import java.awt.event.*;

public class AddUserDialog extends JDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField usernameField;
    private JTextField passwordField;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private static admin a;
    private static User u;



    public AddUserDialog(admin admin, User user){
        this();
        u = user;
        a = admin;
    }


    public AddUserDialog() {
        setTitle("Add User");
        setBounds(100, 100, 450, 234);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);



        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                User newUser = new User();
                String uName = usernameField.getText();
                String uPass = passwordField.getText();
                if(uName.equals("") || uPass.equals("")){
                    JOptionPane.showMessageDialog(AddUserDialog.this, "Error: " + "UserName and Password Cannot be empty!" , "Error", JOptionPane.ERROR_MESSAGE);
                }
                else{
                    newUser.createUser(uName, uPass);
                    JOptionPane.showMessageDialog(AddUserDialog.this, "User: " + uName + "\nSuccessfully added!" , "Success", JOptionPane.PLAIN_MESSAGE);

                }
            }
        });
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        AddUserDialog dialog = new AddUserDialog(a, u);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
