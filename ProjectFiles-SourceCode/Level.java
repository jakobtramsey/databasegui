import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.*;
import java.util.*;

public class Level {
    //Class members
    private int levelID;
    private String name;
    private String creator;
    private int likes;
    private int difficulty;

    //Stuff for database connection

    Properties prop = new Properties();



    private final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String DB_URL;
    private final String USER;
    private final String PASS;
    private Connection connection = null;
    private Statement stmt = null;

    public Level() {
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        this.levelID = 0;
        this.name = "";
        this.creator = "";
        this.likes = 0;
        this.difficulty = 0;
    }

    private Level(int levelID, String name, String creator, int likes, int difficulty) {
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        this.levelID = levelID;
        this.name = name;
        this.creator = creator;
        this.likes = likes;
        this.difficulty = difficulty;
    }

    public int getLevelID() {
        return this.levelID;
    }

    public String getCreator() {
        return this.creator;
    }

    public String getName() {
        return this.name;
    }

    public int getLikes() {
        return this.likes;
    }

    public int getDifficulty() {
        return this.difficulty;
    }

    public void printAllLevels() {
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement playersql = connection.prepareStatement("SELECT * FROM Level");
            ResultSet stream = playersql.executeQuery();

            if (!stream.next()) {
                System.err.println("No uploaded levels");
                connection.close();
                return;
            }

            int count = 1;
            do {
                System.out.println("Level #" + count
                        + ":\tLevelID: " + stream.getInt("LevelID")
                        + "\tName: " + stream.getString("Name")
                        + "\t\tCreator: " + stream.getString("Creator")
                        + "\t\tDifficulty: " + stream.getInt("Difficulty")
                        + "\t\tLikes: " + stream.getInt("Likes"));
                count++;
            } while (stream.next());
            connection.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    connection.close();
                }
            } catch (SQLException se) {/*DO NOTHING*/}
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }

    public List<Level> getAllLevels() throws Exception {
        List<Level> list = new ArrayList<>();

        ResultSet rs = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();
            rs = stmt.executeQuery("SELECT * FROM LeveL");

            while (rs.next()) {
                Level tempLevel = convertToLevel(rs);
                list.add(tempLevel);
            }
            return list;
        } finally {
           connection.close();
        }

    }

    public List<Level> searchLevelsByName(String levelName) throws Exception {

        List<Level> list = new ArrayList<>();
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            levelName += "%";
            PreparedStatement state = connection.prepareStatement("SELECT * FROM Level WHERE Name LIKE ?");

            state.setString(1, levelName);
            ResultSet rs = state.executeQuery();

            while (rs.next()) {
                Level tempLevel = convertToLevel(rs);
                list.add(tempLevel);
            }
            return list;

        } finally {
           connection.close();
        }
    }

    public List<Level> searchLevelsByCreator(String creatorName) throws Exception {

        List<Level> list = new ArrayList<>();
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            creatorName += "%";
            PreparedStatement state = connection.prepareStatement("SELECT * FROM LEVEL WHERE Creator LIKE ?");

            state.setString(1, creatorName);
            ResultSet rs = state.executeQuery();

            while (rs.next()) {
                Level tempLevel = convertToLevel(rs);
                list.add(tempLevel);
            }
            return list;

        } finally {
            connection.close();
        }
    }

    public void printLevslbyUser(String username)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelsbyusersql = connection.prepareStatement("SELECT * FROM Level WHERE Creator = (?)");
            levelsbyusersql.setString(1, username);
            ResultSet stream = levelsbyusersql.executeQuery();

            if(!stream.next())
            {
                System.err.println("This users has created no levels");
                connection.close();
                return;
            }

            int count = 1;

            System.out.println("levels uploaded by "+username+":\n");
            do
            {
                System.out.println(count
                        + ": LevelID: "+stream.getInt("LevelID")
                        + "\tName: "+stream.getString("Name")
                        + "\tCreator: " +stream.getString("Creator"));
                count++;
            }while(stream.next());
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }

    private Level convertToLevel(ResultSet rs) throws SQLException {

        int levelID = rs.getInt("LevelID");
        String name = rs.getString("Name");
        String creator = rs.getString("Creator");
        int likes = rs.getInt("Likes");
        int diff = rs.getInt("Difficulty");

        Level tempLevel = new Level(levelID, name, creator, likes, diff);

        return tempLevel;
    }

    public boolean uploadLevel(User uploaduser, String levelname)
    {
        try
        {
            if(levelname.length() > 20)
            {
                System.err.println("Level name exceeds 20 characters.");
                return false;
            }

            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            String latestIDsql = "SELECT MAX(LevelID) FROM Level";
            ResultSet latestID = stmt.executeQuery(latestIDsql);

            int levelID = 1;

            if(latestID.next())
            {
                levelID = latestID.getInt("MAX(LevelID)") + 1;
            }

            PreparedStatement createlevelsql = connection.prepareStatement("INSERT INTO Level VALUES((?),(?),(?),(?),(?))");
            createlevelsql.setInt(1, levelID);
            createlevelsql.setString(2, levelname);
            createlevelsql.setString(3, uploaduser.getUserName());
            createlevelsql.setInt(4, 0);
            createlevelsql.setInt(5, 0);

            createlevelsql.executeUpdate();
            connection.close();
            return true;
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public boolean deleteLevel(int levelID)
    {
        try
        {
            Class.forName(JDBC_DRIVER);

            System.out.println("Deleting level");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();


            PreparedStatement testsql = connection.prepareStatement("SELECT LevelID, Name FROM Level WHERE LevelID = (?)");
            testsql.setInt(1, levelID);
            ResultSet result = testsql.executeQuery();

            if(!result.next())
            {
                System.err.println("Level with LevelID: " +levelID+ " Does not exists.");
                return false;
            }

            PreparedStatement removesql = connection.prepareStatement("DELETE FROM Level WHERE LevelID = (?)");
            removesql.setInt(1, levelID);
            removesql.executeUpdate();

            System.out.println("Removed level with,\nUserID: "+result.getInt("LevelID")+"\nUsername: "+result.getString("Name"));
            connection.close();
            return true;
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public boolean likeLevel(User likeuser, int levelID, short like)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelexistsql = connection.prepareStatement("SELECT LevelID FROM Level WHERE LevelID = (?)");
            levelexistsql.setInt(1, levelID);
            ResultSet levelstream = levelexistsql.executeQuery();

            if(!levelstream.next())
            {
                System.err.println("Level does not exist");
                return false;
            }


            PreparedStatement likeexistsql = connection.prepareStatement("SELECT * FROM LikeDislike WHERE LevelID = (?) AND UserID = (?)");
            likeexistsql.setInt(1, levelID);
            likeexistsql.setInt(2, likeuser.getUserID());
            ResultSet likestream = likeexistsql.executeQuery();

            if(likestream.next())
            {
                System.err.println("User has already liked or disliked this level.");
                connection.close();
                return false;
            }
            else
            {
                PreparedStatement likesql = connection.prepareStatement("INSERT INTO LikeDislike VALUES((SELECT UserID FROM Player WHERE UserID = (?)), (SELECT LevelID FROM Level WHERE LevelID = (?)), (?))");
                likesql.setInt(1, likeuser.getUserID());
                likesql.setInt(2, levelID);
                likesql.setShort(3, like);
                likesql.executeUpdate();
                connection.close();
                return true;
            }
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public boolean rateLevel(User rateuser, int levelID, short rate)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelexistsql = connection.prepareStatement("SELECT LevelID FROM Level WHERE LevelID = (?)");
            levelexistsql.setInt(1, levelID);
            ResultSet levelstream = levelexistsql.executeQuery();

            if(!levelstream.next())
            {
                System.err.println("Level does not exist");
                return false;
            }


            PreparedStatement likeexistsql = connection.prepareStatement("SELECT * FROM Rates WHERE LevelID = (?) AND UserID = (?)");
            likeexistsql.setInt(1, levelID);
            likeexistsql.setInt(2, rateuser.getUserID());
            ResultSet likestream = likeexistsql.executeQuery();

            if(likestream.next())
            {
                System.err.println("User has already rated this level.");
                connection.close();
                return false;
            }
            else
            {
                PreparedStatement likesql = connection.prepareStatement("INSERT INTO Rates VALUES((SELECT UserID FROM Player WHERE UserID = (?)), (SELECT LevelID FROM Level WHERE LevelID = (?)), (?))");
                likesql.setInt(1, rateuser.getUserID());
                likesql.setInt(2, levelID);
                likesql.setShort(3, rate);
                likesql.executeUpdate();

                connection.close();
                return true;
            }
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public void updateLikeCount()
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            String allLevelsSql = "SELECT LevelID, Likes FROM Level"; //GETS ALL LEVELS
            ResultSet allLevels = stmt.executeQuery(allLevelsSql);

            if(!allLevels.next())
            {
                System.err.println("There are no levels on the database.");
            }
            else
            {
                do //WILL LOOP FOR EVERY EXISTING LEVEL IN THE LEVEL TABLE
                {

                    int likecount = 0;
                    PreparedStatement levelLikeCount = connection.prepareStatement("SELECT LikeCount FROM LikeDislike WHERE LevelID = (?)"); //GETS ALL THE LIKES FOR A GIVEN LEVEK
                    levelLikeCount.setInt(1, allLevels.getInt("LevelID"));
                    ResultSet likesFromLevel = levelLikeCount.executeQuery();

                    while (likesFromLevel.next())
                    {
                        likecount += likesFromLevel.getInt("LikeCount");
                    }

                    PreparedStatement updateLikes = connection.prepareStatement("UPDATE Level SET Likes = (?) WHERE LevelID = (?)");
                    updateLikes.setInt(1, likecount);
                    updateLikes.setInt(2, allLevels.getInt("LevelID"));
                    updateLikes.executeUpdate();

                }while (allLevels.next());
                System.out.println("Done.");
            }
             connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }

    public void updateDifficulty()
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            String allLevelsSql = "SELECT LevelID, Difficulty FROM Level"; //GETS ALL LEVELS
            ResultSet allLevels = stmt.executeQuery(allLevelsSql);

            if(!allLevels.next())
            {
                System.err.println("There are no levels on the database.");
            }
            else
            {
                do //WILL LOOP FOR EVERY EXISTING LEVEL IN THE LEVEL TABLE
                {

                    int rating = 0;
                    int numOfRatings = 0;
                    PreparedStatement levelLikeCount = connection.prepareStatement("SELECT Rating FROM Rates WHERE LevelID = (?)"); //GETS ALL THE LIKES FOR A GIVEN LEVEK
                    levelLikeCount.setInt(1, allLevels.getInt("LevelID"));
                    ResultSet ratesFromLevel = levelLikeCount.executeQuery();

                    while (ratesFromLevel.next())
                    {
                        rating += ratesFromLevel.getInt("Rating");
                        numOfRatings++;
                    }

                    try
                    {
                        rating = rating / numOfRatings;
                    }
                    catch(ArithmeticException e)
                    {
                        rating = 0;
                    }
                    PreparedStatement updateLikes = connection.prepareStatement("UPDATE Level SET Difficulty = (?) WHERE LevelID = (?)");
                    updateLikes.setInt(1, rating);
                    updateLikes.setInt(2, allLevels.getInt("LevelID"));
                    updateLikes.executeUpdate();

                }while (allLevels.next());
                System.out.println("Done.");
            }
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }

    public void allLevelsByDifficulty(short difficulty)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelsByDifficultySql = connection.prepareStatement("SELECT * FROM Level WHERE Difficulty = (?)");
            levelsByDifficultySql.setShort(1, difficulty);
            ResultSet stream = levelsByDifficultySql.executeQuery();

            if(!stream.next())
            {
                System.err.println("There are no levels with this rating.");
                connection.close();
                return;
            }

            int count = 1;

            System.out.println("levels with a "+difficulty+" point rating:\n");
            do
            {
                System.out.println(count
                        + ": LevelID: "+stream.getInt("LevelID")
                        + "\tName: "+stream.getString("Name")
                        + "\tCreator: " +stream.getString("Creator")
                        + "\tLikes: " +stream.getShort("Likes")
                        + "\tDifficulty: " +stream.getShort("Difficulty"));
                count++;
            }while(stream.next());
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }

    public boolean setLevelPercentage(User user, int levelID, short percentage)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelexistsql = connection.prepareStatement("SELECT LevelID FROM Level WHERE LevelID = (?)");
            levelexistsql.setInt(1, levelID);
            ResultSet levelstream = levelexistsql.executeQuery();

            if(!levelstream.next())
            {
                System.err.println("Level does not exist");
                return false;
            }

            PreparedStatement completeExistSql = connection.prepareStatement("SELECT * FROM Percentages WHERE LevelID = (?) AND UserID = (?)");
            completeExistSql.setInt(1, levelID);
            completeExistSql.setInt(2, user.getUserID());
            ResultSet levelProgress = completeExistSql.executeQuery();

            if(levelProgress.next()) //UPDATE PERCENTAGE
            {
                System.out.println("User has already started progress, updating progress...");
                PreparedStatement updatePercentageSql = connection.prepareStatement("UPDATE Percentages SET Percent = (?) WHERE LevelID = (?) AND UserID = (?)");
                updatePercentageSql.setInt(1, percentage);
                updatePercentageSql.setInt(2, levelID);
                updatePercentageSql.setInt(3, user.getUserID());
                updatePercentageSql.executeUpdate();
                connection.close();
                return true;
            }
            else
            {
                PreparedStatement percentageSql = connection.prepareStatement("INSERT INTO Percentages VALUES((SELECT UserID FROM Player WHERE UserID = (?)), (SELECT LevelID FROM Level WHERE LevelID = (?)), (?))");
                percentageSql.setInt(1, user.getUserID());
                percentageSql.setInt(2, levelID);
                percentageSql.setShort(3, percentage);
                percentageSql.executeUpdate();

                connection.close();
                return true;
            }
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public void updatePointCount()
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);

            PreparedStatement allPlayersSql = connection.prepareStatement("SELECT UserID, Points FROM Player");
            ResultSet allPlayers = allPlayersSql.executeQuery();

            PreparedStatement allRatedLevelsSql = connection.prepareStatement("SELECT LevelID, Difficulty FROM Level WHERE Difficulty != 0");
            ResultSet allRatedLevels = allRatedLevelsSql.executeQuery();

            if(!allRatedLevels.next() || !allPlayers.next())
            {
                System.err.println("There are no rated levels or players on the database.");
            }
            else
            {
                do //WILL LOOP FOR EVERY EXISTING LEVEL IN THE LEVEL TABLE
                {
                    int totalpoints = 0;
                    PreparedStatement playersWith100Sql = connection.prepareStatement("SELECT * FROM Percentages WHERE Percent = 100 AND UserID = (?)");
                    playersWith100Sql.setInt(1, allPlayers.getInt("userID"));
                    ResultSet playersWith100 = playersWith100Sql.executeQuery();

                    while(playersWith100.next())
                    {
                        //Get the point count of each level they beat
                        PreparedStatement pointCountSql = connection.prepareStatement("SELECT Difficulty FROM Level WHERE LevelID=(?)");
                        pointCountSql.setInt(1, playersWith100.getInt("LevelID"));
                        ResultSet pointCount = pointCountSql.executeQuery();
                        if(pointCount.next())
                        {
                            totalpoints += pointCount.getInt("Difficulty");
                        }

                    }
                    PreparedStatement updatePointSql = connection.prepareStatement("UPDATE Player SET Points = (?) WHERE UserID = (?)");
                    updatePointSql.setInt(1, totalpoints);
                    updatePointSql.setInt(2, allPlayers.getInt("UserID"));
                    updatePointSql.executeUpdate();
                }while (allPlayers.next());
                System.out.println("Done.");
            }
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }
}
