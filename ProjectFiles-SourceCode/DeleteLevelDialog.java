import javax.swing.*;
import java.awt.event.*;

public class DeleteLevelDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField delLevelField;
    private JLabel levelIDLabel;
    private static admin a;
    private static Level l;



    public DeleteLevelDialog(admin admn, Level level){
        this();
        a = admn;
        l = level;
    }


    public DeleteLevelDialog() {
        setTitle("Delete Level");
        setBounds(200, 200, 350, 234);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int levelID;
                try {
                    levelID = Integer.parseInt(delLevelField.getText());

                    Level removelevel = new Level();

                    if (!removelevel.deleteLevel(levelID)) {
                        JOptionPane.showMessageDialog(DeleteLevelDialog.this, "Error: " + "Level does not exist", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    else{
                        JOptionPane.showMessageDialog(DeleteLevelDialog.this, "Level successfully deleted", "Error", JOptionPane.PLAIN_MESSAGE);

                    }

                }catch(NumberFormatException exc){
                    JOptionPane.showMessageDialog(DeleteLevelDialog.this, "Error: " + "Must enter a proper level ID", "Error", JOptionPane.ERROR_MESSAGE);

                }
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        DeleteLevelDialog dialog = new DeleteLevelDialog(a, l);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
