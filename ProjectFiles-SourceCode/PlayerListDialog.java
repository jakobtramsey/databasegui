import javax.swing.*;
import java.awt.event.*;
import java.util.*;

public class PlayerListDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTable playerTable;
    private JButton buttonCancel;
    private JScrollPane playerScroller;
    private static admin a;
    private static User u;




    public PlayerListDialog(admin admn, User user){
        this();
        a = admn;
        u = user;
    }


    public PlayerListDialog() {
        setTitle("Players");
        setBounds(100, 100, 450, 234);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    User player = new User();
                    List<User> players = player.getAllUsers();

                    PlayerTable pTable = new PlayerTable(players);
                    playerTable.setModel(pTable);

                }catch (Exception exc){
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(PlayerListDialog.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
    }

    private void onOK() {
        // add your code here
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        PlayerListDialog dialog = new PlayerListDialog(a, u);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
