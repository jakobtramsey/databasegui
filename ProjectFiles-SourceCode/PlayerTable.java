import javax.swing.table.AbstractTableModel;
import java.util.*;

public class PlayerTable extends AbstractTableModel {

    private static final int userIDCol = 0;
    private static final int userNameCol = 1;
    private static final int userPointsCol = 2;

    private String[] columnNames = { "UserID", "Username", "Points"};
    private List<User> users;


    public PlayerTable(List<User> Users){
        users = Users;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public int getRowCount() {
        return users.size();
    }
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    @Override
    public Object getValueAt(int row, int col) {
        User tempUser = users.get(row);

        return switch (col) {
            case userIDCol -> tempUser.getUserID();
            case userNameCol -> tempUser.getUserName();
            case userPointsCol -> tempUser.getPoints();
            default -> tempUser.getUserName();
        };
    }
    @Override
    public Class getColumnClass(int c){
        return getValueAt(0, c).getClass();
    }
}
