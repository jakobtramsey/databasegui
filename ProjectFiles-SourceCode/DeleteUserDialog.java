import javax.swing.*;
import java.awt.event.*;


public class DeleteUserDialog extends JDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField userIDField;
    private JLabel userIDLabel;
    private static admin a;
    private static User u;


    public DeleteUserDialog(admin admin, User user){
        this();
        a = admin;
        u = user;
    }


    public DeleteUserDialog() {
        setTitle("Delete User");
        setBounds(100, 100, 450, 234);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               User delUser = new User();
               int id;
               try {
                   id = Integer.parseInt(userIDField.getText());
                   if (!delUser.deleteUser(id)) {
                       JOptionPane.showMessageDialog(DeleteUserDialog.this, "Error: " + "User does not exist!" , "Error", JOptionPane.ERROR_MESSAGE);
                   }
                   JOptionPane.showMessageDialog(DeleteUserDialog.this, "User Successfully Deleted" , "Success", JOptionPane.PLAIN_MESSAGE);

               } catch(NumberFormatException exc){
                   JOptionPane.showMessageDialog(DeleteUserDialog.this, "Error: " + "Field cannot be empty!" , "Error", JOptionPane.ERROR_MESSAGE);

               }

            }
        });
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        DeleteUserDialog dialog = new DeleteUserDialog(a, u);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
