import javax.swing.*;
import java.awt.event.*;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.*;

public class LevelFunctionsDialog extends JDialog {

    Properties prop = new Properties();

    //Stuff for database connection
    private final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private String DB_URL;
    private String USER;
    private String PASS;
    private Connection connection = null;
    private Statement stmt = null;

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton uploadLevelButton;
    private JButton likeLevelButton;
    private JButton rateLevelButton;
    private JButton adjustLevelDifficultyButton;
    private JTextField uploadLevelField;
    private JTextField LikeID;
    private JTextField LevelLikeField;
    private JTextField RateID;
    private JTextField LevelRateField;
    private JTextField DiffID;
    private JTextField LevelDiffField;
    private static admin a;
    private static Level l;
    private static User u;
    private static player p;


    public LevelFunctionsDialog(admin admn, Level level) {
        this();
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        a = admn;
        l = level;
    }

    public LevelFunctionsDialog(player play, Level level){
        this();
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        p = play;
        l = level;
    }

    public LevelFunctionsDialog() {
        setTitle("Level Functions");
        setBounds(500, 500, 500, 800);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onOK();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        uploadLevelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginDialog login = new LoginDialog(LevelFunctionsDialog.this, u);
                login.setVisible(true);
                try {
                    String username = login.getUsername();
                    String password = login.getPassword();
                    String levelname;
                    Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                    User newuser = new User();
                    Level newlevel = new Level();


                    newuser = newuser.login(username, password, connection);


                    if (newuser.getUserID() == 0) {
                        JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Something went Wrong", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        levelname = uploadLevelField.getText();
                        if (!newlevel.uploadLevel(newuser, levelname)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Couldn't upload level \n Level already exists", "Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Level upload successful", "Success", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                } catch (Exception exc) {
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });


        likeLevelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginDialog login = new LoginDialog(LevelFunctionsDialog.this, u);
                login.setVisible(true);

                try {
                    String username = login.getUsername();
                    String password = login.getPassword();
                    int levelID;
                    short like;
                    try{
                        levelID = Integer.parseInt(LikeID.getText());
                        like = Short.parseShort(LevelLikeField.getText());
                    }catch(NumberFormatException exc){
                        JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must be a numeric value", "Error", JOptionPane.ERROR_MESSAGE);

                    }

                    levelID = Integer.parseInt(LikeID.getText());
                    like = Short.parseShort(LevelLikeField.getText());

                    Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                    User loginuser = new User();
                    Level likelevel = new Level();

                    loginuser = loginuser.login(username, password, connection);


                    if (loginuser.getUserID() == 0) {
                        JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Something went Wrong", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        if ((like > 1) || (like < -1)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must Be either -1, or 1", "Error", JOptionPane.ERROR_MESSAGE);
                        }

                        if (!likelevel.likeLevel(loginuser, levelID, like)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Couldn't like/dislike successfully \nLevel doesn't exist", "Error", JOptionPane.ERROR_MESSAGE);

                        } else {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Level liked/disliked Successfully!", "Success", JOptionPane.PLAIN_MESSAGE);
                        }
                    }

                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must enter proper values", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        });


        rateLevelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginDialog login = new LoginDialog(LevelFunctionsDialog.this, u);
                login.setVisible(true);
                try {
                    String username = login.getUsername();
                    String password = login.getPassword();
                    int levelID = Integer.parseInt(RateID.getText());
                    short rate = Short.parseShort(LevelRateField.getText());
                    Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                    User loginuser = new User();
                    Level ratelevel = new Level();

                    loginuser = loginuser.login(username, password, connection);

                    if (loginuser.getUserID() == 0) {
                        JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Something went wrong", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        if ((rate < 1) || (rate > 10)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must be a number 1 - 10", "Error", JOptionPane.ERROR_MESSAGE);
                        }

                        if (!ratelevel.rateLevel(loginuser, levelID, rate)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Couldn't rate level \n Level does not exist", "Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Level rated successfully", "Success", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                }catch(Exception exc){
                    JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must enter correct values", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        });



        adjustLevelDifficultyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LoginDialog login = new LoginDialog(LevelFunctionsDialog.this, u);
                login.setVisible(true);
                try {
                    String username = login.getUsername();
                    String password = login.getPassword();
                    int levelID = Integer.parseInt(DiffID.getText());
                    short percentage = Short.parseShort(LevelDiffField.getText());

                    Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                    User loginuser = new User();
                    Level playedlevel = new Level();

                    loginuser = loginuser.login(username, password, connection);

                    if (loginuser.getUserID() == 0) {
                        JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "UserName or Password was Incorrect or User Doesn't exist", "Error", JOptionPane.ERROR_MESSAGE);
                    } else {
                        if ((percentage < 0) || (percentage > 100)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must be a number 0 - 100", "Error", JOptionPane.ERROR_MESSAGE);
                        }

                        if (!playedlevel.setLevelPercentage(loginuser, levelID, percentage)) {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Level Doesn't Exist \nCouldn't set percentage level", "Error", JOptionPane.ERROR_MESSAGE);
                        } else {
                            JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Level difficulty successfully set", "Success", JOptionPane.PLAIN_MESSAGE);
                        }
                    }
                }catch(Exception exc){
                    JOptionPane.showMessageDialog(LevelFunctionsDialog.this, "Error: " + "Must enter correct values", "Error", JOptionPane.ERROR_MESSAGE);

                }
            }
        });

        uploadLevelField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                uploadLevelField.setText("");
            }
        });

        LikeID.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                LikeID.setText("");
            }
        });




        RateID.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                RateID.setText("");
            }
        });

        LevelLikeField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                LevelLikeField.setText("");
            }
        });

        DiffID.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                DiffID.setText("");
            }

        });

        LevelRateField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                LevelRateField.setText("");
            }
        });

        LevelDiffField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                LevelDiffField.setText("");
            }
        });

    }

        private void onOK () {
            dispose();
        }

        public static void main (String[]args){
            LevelFunctionsDialog dialog = new LevelFunctionsDialog(a, l);
            LevelFunctionsDialog dialog2 = new LevelFunctionsDialog(p, l);
            dialog.pack();
            dialog.setVisible(true);
            System.exit(0);
        }
    }
