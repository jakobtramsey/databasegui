import javax.swing.table.AbstractTableModel;
import java.util.*;

public class LevelTable extends AbstractTableModel {

    private static final int levelIDCol = 0;
    private static final int levelNameCol = 1;
    private static final int creatorCol = 2;
    private static final int likesCol = 3;
    private static final int difficultyCol = 4;

    private String[] columnNames = { "LevelID", "Level Name", "Creator", "Likes", "Difficulty" };
    private List<Level> levels;


    public LevelTable(List<Level> Levels){
        levels = Levels;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }
    @Override
    public int getRowCount() {
        return levels.size();
    }
    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }
    @Override
    public Object getValueAt(int row, int col) {
        Level tempLevel = levels.get(row);

        return switch (col) {
            case levelIDCol -> tempLevel.getLevelID();
            case levelNameCol -> tempLevel.getName();
            case creatorCol -> tempLevel.getCreator();
            case likesCol -> tempLevel.getLikes();
            case difficultyCol -> tempLevel.getDifficulty();
            default -> tempLevel.getName();
        };
    }
    @Override
    public Class getColumnClass(int c){
        return getValueAt(0, c).getClass();
        }
    }
