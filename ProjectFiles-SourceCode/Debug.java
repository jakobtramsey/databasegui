import java.sql.*;
import java.util.Scanner;

public class Debug
{
    private final static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final static String DB_URL = "jdbc:mysql://localhost:3306/DBproj";
    private final static String USER = "root";
    private final static String PASS = "Combatarms55";
    private Connection connection = null;

    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        Scanner keyboardspace = new Scanner(System.in);
        keyboardspace.useDelimiter("\n");
        int option;

        System.out.println("\t\tGeometry Jump Debug Console\n" +
                "Here is a helpful set of tools for testing database features for new upcoming game\n\n" +
                "\t1) Return a list of all registered players.\n" +
                "\t2) Return a list of all upload levels.\n" +
                "\t3) Return levels created by specific user.\n" +
                "\t4) Return the stats of a certain user.\n" +
                "\t5) Like a level as a user(Requires login).\n" +
                "\t6) Rate a level as a user(Requires login).\n" +
                "\t7) Upload a level as a user(Requires login).\n" +
                "\t8) Delete a level.\n" +
                "\t9) Set level completion percentage(Requires login).\n" +
                "\t10) Return top 3 players.\n" +
                "\t11) Return all levels of a certain difficulty\n" +
                "\t12) Create a new user.\n" +
                "\t13) Remove a user.\n" +
                "\t14) Update like count for all levels.\n" +
                "\t15) Update difficulty for all levels.\n" +
                "\t16) Update point count for all players.\n" +
                "\t17) Exit.\n");
        do
        {
            System.out.println("Which command would you like to run? (1-14)");

            try
            {
                option = keyboard.nextInt();

                if(option < 1 || option > 17)
                {
                    System.err.println("Input out of range, exiting...");
                    System.exit(2);
                }

                switch (option) {
                    case 1 -> //Prints all users
                            {
                                User printuser = new User();

                                System.out.println("\tGetting all users");
                                printuser.printAllUsers();
                                break;
                            }
                    case 2 -> //Prints all levels
                            {
                                Level printlevel = new Level();

                                System.out.println("\tGetting all levels");
                                printlevel.printAllLevels();
                                break;
                            }
                    case 3 -> //Gets all levels printed by a specific user
                            {
                                String username;
                                Level printlevel = new Level();

                                keyboard = new Scanner(System.in);

                                System.out.println("\tGet all levels by a certain user\nEnter a username");

                                try
                                {
                                    username = keyboard.next();
                                    printlevel.printLevslbyUser(username);
                                }
                                catch(NumberFormatException ne)
                                {
                                    System.err.println("input was not a number");
                                }
                                break;
                            }
                    case 4 -> //Returns the stats of a certain user
                            {
                                String username;
                                User printUserStats = new User();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tGet stats of a certain user\nEnter a username");
                                try
                                {
                                    username = keyboard.next();
                                    printUserStats.printStats(username);
                                }
                                catch(NumberFormatException ne)
                                {
                                    System.err.println("input was not a number");
                                }
                                break;
                            }
                    case 5 -> //Like a level as a user with login
                            {
                                String username;
                                String password;
                                int levelID;
                                short like;
                                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                                User loginuser = new User();
                                Level likelevel = new Level();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tLike/dislike level as user.\nEnter a username and password(Separated by spaces).");
                                username = keyboard.next();
                                password = keyboard.next();

                                loginuser = loginuser.login(username, password, connection);


                                if(loginuser.getUserID() == 0)
                                {
                                    System.err.println("Something went wrong.");
                                }
                                else
                                {
                                    System.out.println("Enter a level id and the rating to give it(-1 through 1):");
                                    levelID = keyboard.nextInt();
                                    like = keyboard.nextShort();

                                    if((like > 1) || (like < -1))
                                    {
                                        System.err.println("Must be a number -1 through 1.");
                                        break;
                                    }

                                    if(!likelevel.likeLevel(loginuser, levelID, like))
                                    {
                                        System.err.println("Couldn't like/disliked level.");
                                    }
                                    else
                                    {
                                        System.out.println("Level liked/disliked successfully.");
                                    }
                                }
                                break;
                            }
                    case 6 -> //Rate a level as a user(Requires login)
                            {
                                String username;
                                String password;
                                int levelID;
                                short rate;
                                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                                User loginuser = new User();
                                Level ratelevel = new Level();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tRate level as user.\nEnter a username and password(Separated by spaces).");
                                username = keyboard.next();
                                password = keyboard.next();

                                loginuser = loginuser.login(username, password, connection);

                                if(loginuser.getUserID() == 0)
                                {
                                    System.err.println("Something went wrong.");
                                }
                                else
                                {
                                    System.out.println("Enter a level id and the rating to give it(1 through 10):");
                                    levelID = keyboard.nextInt();
                                    rate = keyboard.nextShort();

                                    if((rate < 1) || (rate > 10))
                                    {
                                        System.err.println("Must be a number 1 through 10.");
                                        break;
                                    }

                                    if(!ratelevel.rateLevel(loginuser, levelID, rate))
                                    {
                                        System.err.println("Couldn't rate level.");
                                    }
                                    else
                                    {
                                        System.out.println("Level rated successfully");
                                    }
                                }
                                break;
                            }
                    case 7 -> //Upload a level as a user(Requires login)
                            {
                                String username;
                                String password;
                                String levelname;
                                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                                User newuser = new User();
                                Level newlevel = new Level();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tUpload a level\nEnter a username and password(Separated by spaces)");
                                username = keyboard.next();
                                password = keyboard.next();

                                newuser = newuser.login(username, password, connection);


                                if(newuser.getUserID() == 0)
                                {
                                    System.err.println("Something went wrong.");
                                }
                                else
                                {
                                    System.out.println("Enter a level name: ");
                                    levelname = keyboardspace.next();

                                    if(!newlevel.uploadLevel(newuser, levelname))
                                    {
                                        System.err.println("Couldn't upload level.");
                                    }
                                    else
                                    {
                                        System.out.println("Level upload successful");
                                    }
                                }
                                break;
                            }
                    case 8 -> //Delete a level
                            {
                                int levelID;
                                keyboard = new Scanner(System.in);
                                Level removelevel = new Level();

                                System.out.println("\tRemove a level\nEnter the LevelID of level to remove: ");
                                levelID = keyboard.nextInt();

                                if (!removelevel.deleteLevel(levelID)) {
                                    System.err.println("Unable to remove level.");
                                }

                                break;
                            }
                    case 9 -> //Set level completion percentage(Requires login)
                            {
                                String username;
                                String password;
                                int levelID;
                                short percentage;
                                Connection connection = DriverManager.getConnection(DB_URL, USER, PASS);
                                User loginuser = new User();
                                Level playedlevel = new Level();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tSet percentage for level\nEnter a username and password(Separated by spaces)");
                                username = keyboard.next();
                                password = keyboard.next();

                                loginuser = loginuser.login(username, password, connection);

                                if(loginuser.getUserID() == 0)
                                {
                                    System.err.println("Something went wrong.");
                                }
                                else
                                {
                                    System.out.println("Enter a level id and the percentage earned(0 through 100):");
                                    levelID = keyboard.nextInt();
                                    percentage = keyboard.nextShort();

                                    if((percentage < 0) || (percentage > 100))
                                    {
                                        System.err.println("Must be a number 1 through 100.");
                                        break;
                                    }

                                    if(!playedlevel.setLevelPercentage(loginuser, levelID, percentage))
                                    {
                                        System.err.println("Couldn't set percentage level.");
                                    }
                                    else
                                    {
                                        System.out.println("Percentage successfully set");
                                    }
                                }
                                break;
                            }
                    case 10 -> //Return top 3 players.
                            {
                                User top3User = new User();

                                System.out.println("Getting top 3 players.");
                                top3User.top3Players();
                                break;
                            }
                    case 11 -> //Return all levels of a certain difficulty
                            {
                                Short difficulty;
                                Level printlevel = new Level();
                                keyboard = new Scanner(System.in);

                                System.out.println("\tGet all levels with a certain diffculty\nEnter a difficulty");
                                try
                                {
                                    difficulty = keyboard.nextShort();
                                    printlevel.allLevelsByDifficulty(difficulty);
                                }
                                catch(NumberFormatException ne)
                                {
                                    System.err.println("input was not a number");
                                }
                                break;
                            }
                    case 12 -> //CREATE A USER
                            {
                                String username;
                                String password;
                                User newuser = new User();

                                keyboard = new Scanner(System.in);

                                System.out.println("\tCreate new user\nEnter a username and password(Separated by spaces)");
                                username = keyboard.next();
                                password = keyboard.next();

                                if (!newuser.createUser(username, password)) {
                                    System.err.println("Unable to create user.");
                                }
                                break;
                            }
                    case 13 -> //REMOVE A USER
                            {
                                int userID;
                                keyboard = new Scanner(System.in);
                                User removeuser = new User();

                                System.out.println("\tRemove a user\nEnter the UserID of user to remove: ");
                                userID = keyboard.nextInt();

                                if (!removeuser.deleteUser(userID)) {
                                    System.err.println("Unable to remove user.");
                                }
                                break;
                            }
                    case 14 -> //Update like count for all levels
                            {
                                System.out.println("\tUpdating likes for every level, this may take a while...");
                                Level updateLikes = new Level();
                                updateLikes.updateLikeCount();
                            }
                    case 15 -> //Update difficulty for all levels
                            {
                                System.out.println("\tUpdating difficulty for every level, this may take a while...");
                                Level updateDifficulty = new Level();
                                updateDifficulty.updateDifficulty();
                            }
                    case 16 -> //Update point count for all levels
                            {
                                System.out.println("\tUpdating point count for every player, this may take a while...");
                                Level updatePointCount = new Level();
                                updatePointCount.updatePointCount();
                            }
                    default -> System.exit(0);
                }
            }
            catch(NumberFormatException | SQLException ne)
            {
                System.err.println("Input was not a number.");
            }

        }while(true);
    }
}
