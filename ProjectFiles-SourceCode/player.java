import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class player extends JFrame {
    User user = new User();
    Level lvl = new Level();


    //gui variables
    private JPanel panel;
    private JButton searchButton;
    private JPanel playerView;
    private JTable levelsTable;
    private JTextField searchTextField;
    private JLabel playerControlPanelLabel;
    private JScrollPane levelsScroller;
    private JButton levelFunctionsButton;
    private JButton updateAllButton;


    public player(){

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    Level level = new Level();
                    String field = searchTextField.getText();
                    List<Level> levels = level.getAllLevels();

                    for(int i = 0; i < levels.size(); i++){
                        if(field.equals(levels.get(i).getName())){
                            levels = level.searchLevelsByName(field);
                        }
                        else if(field.equals(levels.get(i).getCreator())){
                            levels = level.searchLevelsByCreator(field);
                        }
                        else{
                            levels = level.getAllLevels();
                        }
                    }
                    LevelTable ltable = new LevelTable(levels);
                    levelsTable.setModel(ltable);

                } catch (Exception exc){
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(player.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        updateAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Level update = new Level();

                update.updateDifficulty();
                update.updateLikeCount();
                update.updatePointCount();

                JOptionPane.showMessageDialog(player.this, "Levels Updated", "Success", JOptionPane.PLAIN_MESSAGE);

                try{
                    List<Level> levels = update.getAllLevels();
                    LevelTable ltable = new LevelTable(levels);
                    levelsTable.setModel(ltable);
                } catch(Exception exc){
                    JOptionPane.showMessageDialog(player.this, "Couldn't update table", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });
        levelFunctionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LevelFunctionsDialog dialog = new LevelFunctionsDialog(player.this, lvl);
                dialog.setVisible(true);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("player");
        frame.setContentPane(new player().playerView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
