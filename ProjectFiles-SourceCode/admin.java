import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class admin extends JFrame {
    User user = new User();
    Level lvl = new Level();



    //gui variables
    private JPanel panel;
    private JButton searchButton;
    private JPanel adminView;
    private JTable levelsTable;
    private JTextField searchTextField;
    private JLabel adminControlPanelLabel;
    private JScrollPane levelsScroller;
    private JButton addUserButton;
    private JButton deleteUserButton;
    private JButton playerListButton;
    private JButton levelFunctionsButton;
    private JButton updateAllButton;
    private JButton deleteLevelButton;


    public admin(){

        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try{
                    Level level = new Level();
                    String field = searchTextField.getText();
                    List<Level> levels = level.getAllLevels();

                    for(int i = 0; i < levels.size(); i++){
                        if(field.equals(levels.get(i).getName())){
                            levels = level.searchLevelsByName(field);
                        }
                        else if(field.equals(levels.get(i).getCreator())){
                            levels = level.searchLevelsByCreator(field);
                        }
                        else{
                            levels = level.getAllLevels();
                        }
                    }
                  LevelTable ltable = new LevelTable(levels);
                    levelsTable.setModel(ltable);

                } catch (Exception exc){
                    exc.printStackTrace();
                    JOptionPane.showMessageDialog(admin.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        addUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AddUserDialog dialog = new AddUserDialog(admin.this, user);
                dialog.setVisible(true);
            }
        });
        deleteUserButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DeleteUserDialog dialog = new DeleteUserDialog(admin.this, user);
                dialog.setVisible(true);
            }
        });
        playerListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PlayerListDialog dialog = new PlayerListDialog(admin.this, user);
                dialog.setVisible(true);
            }
        });
        updateAllButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Level update = new Level();

               update.updateDifficulty();
               update.updateLikeCount();
               update.updatePointCount();

                JOptionPane.showMessageDialog(admin.this, "Levels Updated", "Success", JOptionPane.PLAIN_MESSAGE);

                try{
                    List<Level> levels = update.getAllLevels();
                    LevelTable ltable = new LevelTable(levels);
                    levelsTable.setModel(ltable);
                } catch(Exception exc){
                    JOptionPane.showMessageDialog(admin.this, "Couldn't update table", "Error", JOptionPane.ERROR_MESSAGE);

                }

            }
        });
        levelFunctionsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                LevelFunctionsDialog dialog = new LevelFunctionsDialog(admin.this, lvl);
                dialog.setVisible(true);
            }
        });
        deleteLevelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                DeleteLevelDialog dialog = new DeleteLevelDialog(admin.this, lvl);
                dialog.setVisible(true);
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("admin");
        frame.setContentPane(new admin().adminView);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
