INSERT INTO Player VALUES(1, "Ryder", "test", 0);
INSERT INTO Player VALUES(2, "Jacob", "apple", 0);
INSERT INTO Player VALUES(3, "Rene", "Taylor", 0);

INSERT INTO Level VALUES(1, "Test", "Ryder", 0, 0);
INSERT INTO Level VALUES(2, "Test2", "Jacob", 0, 0);

INSERT INTO LikeDislike VALUES(1, 1, -1);
INSERT INTO LikeDislike VALUES(1, 2, -1);
INSERT INTO LikeDislike VALUES(2, 1, 1);
INSERT INTO LikeDislike VALUES(2, 2, 1);
INSERT INTO LikeDislike VALUES(3, 1, -1);
INSERT INTO LikeDislike VALUES(3, 2, -1);

INSERT INTO Rates VALUES(1, 1, 5);
INSERT INTO Rates VALUES(1, 2, 4);
INSERT INTO Rates VALUES(2, 1, 10);
INSERT INTO Rates VALUES(2, 2, 9);

INSERT INTO Percentages VALUES(1, 1, 100);
INSERT INTO Percentages VALUES(1, 2, 78);
INSERT INTO Percentages VALUES(2, 1, 0);
INSERT INTO Percentages VALUES(2, 2, 100);