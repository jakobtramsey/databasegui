import java.io.FileInputStream;
import java.sql.*;
import java.util.*;


public class User
{
    Properties prop = new Properties();
    //Class members
    private int userID;
    private String userName;
    private String password;
    private int points;

    //Stuff for database connection
    private  final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    private final String DB_URL;
    private final String USER;
    private final String PASS;
    private Connection connection = null;
    private Statement stmt = null;

    //Public constructors, for non login purposes only
    public User()
    {
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        this.userID = 0;
        this.userName = "";
        this.password = "";
        this.points = 0;
    }

    //Private constructor, for logins only
    private User(int userID, String userName, String password, int points)
    {
        try{
            prop.load(new FileInputStream("databaseConnection.properties"));
        }catch(Exception exc){
            exc.printStackTrace();
        }
        this.DB_URL = prop.getProperty("DB_URL");
        this.USER = prop.getProperty("USER");
        this.PASS = prop.getProperty("PASS");
        this.userID = userID;
        this.userName = userName;
        this.password = password;
        this.points = points;
    }

    public int getUserID()
    {
        return this.userID;
    }

    public String getUserName()
    {
        return this.userName;
    }

    public int getPoints()
    {
        return this.points;
    }

    public boolean createUser(String userName, String password)
    {
        try
        {
            Class.forName(JDBC_DRIVER);

            System.out.println("Creating user");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            if(this.userName.length() > 20 || this.password.length() > 30)
            {
                System.err.println("Malformed Username/password combo, usernames should be no more than 20 characters and" +
                        "passwords should be no more than 30 characters");
                return false;
            }

            int userID = 1;

            PreparedStatement uniqueUsernamesql = connection.prepareStatement("SELECT Username FROM Player WHERE BINARY Username = (?)");
            uniqueUsernamesql.setString(1, userName);
            ResultSet testUsernameresult = uniqueUsernamesql.executeQuery();

            if(testUsernameresult.next())
            {
                System.err.println("Username already exists");
                return false;
            }

            String latestIDsql = "SELECT MAX(UserID) FROM Player";
            ResultSet latestID = stmt.executeQuery(latestIDsql);

            if(latestID.next())
            {
                userID = latestID.getInt("MAX(UserID)") + 1;
            }

            PreparedStatement insertsql = connection.prepareStatement("INSERT INTO Player (UserID, Username, Password, Points) VALUES(?,?,?,?)");
            insertsql.setInt(1, userID);
            insertsql.setString(2, userName);
            insertsql.setString(3, password);
            insertsql.setInt(4, 0);
            insertsql.executeUpdate();

            System.out.println("Created user with,\nUserID: " +userID+ "\nUserName: " +userName+ "\nPassword: " +password);
            connection.close();
            return true;
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public boolean deleteUser(int userID)
    {
        try
        {
            Class.forName(JDBC_DRIVER);

            System.out.println("Deleting user");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();


            PreparedStatement testsql = connection.prepareStatement("SELECT UserID, Username FROM Player WHERE UserID = (?)");
            testsql.setInt(1, userID);
            ResultSet result = testsql.executeQuery();

            if(!result.next())
            {
                System.err.println("User with UserID: " +userID+ " Does not exists.");
                return false;
            }

            PreparedStatement removesql = connection.prepareStatement("DELETE FROM Player WHERE UserID = (?)");
            removesql.setInt(1, userID);
            removesql.executeUpdate();

            System.out.println("Removed user with,\nUserID: "+result.getInt("UserID")+"\nUsername: "+result.getString("Username"));
            connection.close();
            return true;
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
        return false;
    }

    public User login(String username, String password, Connection connection)
    {
        try
        {
            Class.forName(JDBC_DRIVER);

            stmt = connection.createStatement();

            PreparedStatement testsql = connection.prepareStatement("SELECT * FROM Player WHERE Username = (?) AND BINARY Password = (?)");
            testsql.setString(1, username);
            testsql.setString(2, password);
            ResultSet result = testsql.executeQuery();

            if(!result.next())
            {
                System.err.println("Username or password was incorrect or user does not exist.");
                return new User();
            }

            return new User(result.getInt("UserID"), result.getString("Username"),
                    result.getString("password"), result.getInt("points"));
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }

        return new User();
    }

    public void printAllUsers()
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement playersql = connection.prepareStatement("SELECT * FROM Player");
            ResultSet stream = playersql.executeQuery();

            if(!stream.next())
            {
                System.err.println("No Registered players");
                connection.close();
                return;
            }

            int count = 1;
            do
            {
                System.out.println("Player #"+count+ ":\tUserID: "+stream.getInt("UserID")+ "\tUsername: " +stream.getString("Username")+
                        "\t\tPoints: "+stream.getInt("Points"));
                count++;
            }while(stream.next());
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }

    }
    public List<User> getAllUsers() throws Exception {
        List<User> list = new ArrayList<>();

        ResultSet rs = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();
            rs = stmt.executeQuery("SELECT * FROM Player");

            while (rs.next()) {
                User tempUser = convertToUser(rs);
                list.add(tempUser);
            }
            return list;
        } finally {
            connection.close();
        }

    }
    public List<User> searchUsers(String userName) throws Exception {

        List<User> list = new ArrayList<>();
        PreparedStatement state = null;
        ResultSet rs = null;
        try {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            userName += "%";
            state = connection.prepareStatement("SELECT * FROM Player WHERE Username LIKE ?");

            state.setString(1, userName);
            rs = state.executeQuery();

            while (rs.next()) {
                User tempUser = convertToUser(rs);
                list.add(tempUser);
            }
            return list;

        } finally {
            connection.close();
        }
    }

    private User convertToUser(ResultSet rs) throws SQLException {

        int userID = rs.getInt("UserID");
        String userName = rs.getString("Username");
        String password = rs.getString("Password");
        int points = rs.getInt("Points");


        User tempUser = new User(userID,userName,password,points);

        return tempUser;
    }
    public void printStats(String username)
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement levelsbyusersql = connection.prepareStatement("SELECT * FROM Player WHERE Username = (?)");
            levelsbyusersql.setString(1, username);
            ResultSet stream = levelsbyusersql.executeQuery();

            if(!stream.next())
            {
                System.err.println("This users does not exist");
                connection.close();
                return;
            }


            System.out.println("Stats for " +username+":\n");
            do
            {
                System.out.println(
                        "\tUsername: "+stream.getString("Username")
                        + "\tPoints: " +stream.getInt("Points"));
            }while(stream.next());
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }

    public void top3Players()
    {
        try
        {
            Class.forName(JDBC_DRIVER);
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            stmt = connection.createStatement();

            PreparedStatement playersql = connection.prepareStatement("SELECT * FROM Player");
            ResultSet stream = playersql.executeQuery();

            if(!stream.next())
            {
                System.err.println("No Registered players");
                connection.close();
                return;
            }

            PreparedStatement top3PlayersSql = connection.prepareStatement("SELECT Username, Points FROM Player ORDER BY Points DESC");
            ResultSet top3Players = top3PlayersSql.executeQuery();

            for(int i = 1; i < 4; i++)
            {
                if(!top3Players.next())
                {
                    break;
                }
                else
                {
                    System.out.println("#" +i+ ": " +top3Players.getString("Username")+ "\tPoints: " +top3Players.getInt("Points"));
                }
            }
            connection.close();
        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if (stmt != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se) {/*DO NOTHING*/}
            try
            {
                if (connection != null)
                {
                    connection.close();
                }
            }
            catch (SQLException se)
            {
                se.printStackTrace();
            }
        }
    }
}
