import javax.swing.*;
import java.awt.event.*;

public class LoginDialog extends JDialog {

    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField usernameField;
    private JTextField passwordField;
    private JLabel usernameLabel;
    private JLabel passwordLabel;
    private static LevelFunctionsDialog lF;
    private static User u;
    private static String userName;
    private static String passWord;


    public LoginDialog(LevelFunctionsDialog lFunc, User user){
        this();
        u = user;
        lF = lFunc;
    }

    public LoginDialog() {
        setTitle("Add User");
        setBounds(100, 100, 450, 234);
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });


        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String uName = usernameField.getText();
                String uPass = passwordField.getText();
                if(uName.equals("") || uPass.equals("")){
                    JOptionPane.showMessageDialog(LoginDialog.this, "Error: " + "UserName and Password Cannot be empty!" , "Error", JOptionPane.ERROR_MESSAGE);
                }
                else{
                    userName = uName;
                    passWord = uPass;
                }


            }
        });
    }

    private void onOK() {
        dispose();
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        LoginDialog dialog = new LoginDialog(lF, u);
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    public String getUsername(){
        return this.userName;
    }
    public  String getPassword() {
        return this.passWord;
    }
}